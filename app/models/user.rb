class User < ApplicationRecord
    def self.authenticate(uid, pass)
        if user = find_by(uid: uid, pass: pass)
            user
        else
            nil
        end
    end
end
