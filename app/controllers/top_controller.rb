class TopController < ApplicationController
    def main
        if session[:login_uid] != nil
            render '/main'
        else
            render '/login'
        end
    end
    
    def login
        uid = params[:uid]
        pass = params[:pass]
        if User.authenticate(uid, pass)
            session[:login_uid] = uid
            render '/main'
        else
            render '/error'
        end
    end
    
    def logout
        session.delete(:login_uid)
        redirect_to '/'
    end
end
